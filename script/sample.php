<?php
include( "lib/utils.php" );

$path = "/tmp/";
$date = date('Ymd_His');
$dirpath = $path. $date;
print_form();


if( $_POST['keyword'] ){
  $browse_node = $_POST['browse_node'];
  $results = search( $_POST['keyword'] , 1 , $browse_node );
  display_results($results,$dirpath);
  zip_directory($dirpath);
}
exit();

function search($keyword,$page=1,$browse_node=""){
  $result = array();

  $params = array();
  $params['Operation'] = 'ItemSearch'; // ← ItemSearch オペレーションの例
  $params['SearchIndex'] = 'All'; // カテゴリXXX
  //* MerchantId
  // 出品者を表すID。デフォルトでは全ての出品者の商品が対象となるが、「Amazon」を指定することでAmazon出品の商品のみが扱われる。
  //*Condition
  // Newで新品のみ
  //* Availability
  // 在庫有無

  $params['Keywords'] = $keyword; // ← 検索ワード
  $params['ItemPage'] = $page;
  $params['ResponseGroup']= "Images,ItemAttributes,OfferSummary";
  $xml = _call_api($params);
  $hash = json_decode(json_encode($xml), true);
  if(!isset($hash['Items']) ){
    return $result;
  }
  $data = $hash['Items'];
  foreach ( $data['Item'] as $item){
    $result[] = $item;
  }
  print "TotalResults: ". $data['TotalResults'];

  return $result;
}

function item_info($item){
  #print "<pre>"; print_r($item);  print "</pre>";

  $attr    = $item['ItemAttributes'];
  $summary = $item['OfferSummary'];

  $url   = $item['DetailPageURL'];;
  $brand = "";
  $color = "";
  $size  = "";
  $qty   = "";
  $category = "";
  if(isset($attr['ProductGroup']) ){
    $category = $attr['ProductGroup'];
  }
  if(isset($attr['Brand']) ){
    $brand = $attr['Brand'];
  }
  if(isset($attr['Color']) ){
    $Color = $attr['Color'];
  }
  if(isset($attr['Size']) ){
    $size = $attr['Size'];
  }
  if(isset($summary['TotalNew']) ){
    $qty = $summary['TotalNew'];
  }
  $asin  = $item['ASIN'];
  $title = $attr['Title'];
  $price = $attr['ListPrice']['Amount'];
  $str   = "$path,$name,$code,$sub_code";
  #print $head."\n";
  #print $str ."\n";
  #$head = "brand,title,price,qty,color,size,img,category,asin";
  $line = array($url,$brand,$title,$price,
                $qty,$color,$size,
                $category,$asin  );

  foreach (get_image_list($item) as $img){
    $line[] = $img['LargeImage']['URL'];
  } 
  return $line;
}
 
function output_csv($items,$dir){
  
  $head = "url,brand,title,price,qty,color,size,category,asin,img\n";
  $country = $_POST['country'];
  $csvpath = "{$dir}.csv";
  $fp = fopen($csvpath, "w");
  #fputcsv($fp,$head);
  fwrite($fp, $head);
  $str ="";
  foreach ($items as $line){
    #print_r($line);
    foreach ($line as $l){
      #print "** $l <BR/>";
      $str .= "$l,";
    }
    $str .="\n";
    #$line = mb_convert_encoding($line,"SJIS", "UTF-8");
    #fputcsv($fp,$line);
  }
#  print "<BR/>BEFORE : $str";
  #$str = mb_convert_encoding($str, "SJIS","UTF-8");
#  print "<BR/>AFTER $str";
  fwrite($fp, $str);
  fclose($fp);
}

function display_results($results,$dir = "/tmp/20150411/"){
  print "<table border=1>";
  $item_array = array();

  foreach ( $results as $item){
    $item_array[] = item_info($item);
    $ASIN = $item['ASIN'];
    $dirpath = $dir."/". $ASIN;
    make_directory($dirpath);

    print "<tr>";
    print "<td>";
    #print "<pre>";print_r($item);print "</pre>";
    $image_list = get_image_list($item);
    $index = 1;
    foreach ($image_list as $img ){
      do_wget($img,$dirpath,$index);
      $index++;
    }

    print "</td>";
    td( $ASIN );
   td( '<a href="'.$item['DetailPageURL'] . '" target="_blank">'.$item['ItemAttributes']['Title'] .'</a)');
#   td($item['ItemAttributes']['ListPrice']['Amount']);
   td($item['OfferSummary']['LowestNewPrice']['Amount']);
   td($item['OfferSummary']['LowestUsedPrice']['Amount']);

    print "</tr>";
  }
  print "</table>";
  output_csv($item_array,$dir);
  global $date;
  $tmp_csv = $date.".csv";
  $tmp_zip = $date.".zip";
  print '<a href="/download.php?src='.$tmp_csv . '" target="_blank">'.$tmp_csv .'</a><br/>';
  print '<a href="/download.php?src='.$tmp_zip . '" target="_blank">'.$tmp_zip .'</a><br/>';
}

function get_image_list($item){
  $img_list = array();
  if( isset ( $item['ImageSets']['ImageSet']) ){
    if(isset($item['ImageSets']['ImageSet'][0]) ) {//画像が複数枚の場合
      foreach($item['ImageSets']['ImageSet'] as $img){
        $img_list[] = $img;
      }
    }
    else{//画像が１枚の場合
      $img_list[] = $item['ImageSets']['ImageSet'];
    }
  }
  return $img_list;
}
function print_form(){
  print "APIの仕様上100件までの情報しか取得できません";
  print '<form action="./" method="POST">';
  print 'keyword:<input type="text" name="keyword" value=""/><br>';
  print 'browse node : <input type="text" name="browse_node" value=""/><br/>';
  print 'country : <select name="country">
<option value="JP" selected>JP</option>
<option value="US">US</option>
<option value="UK">UK</option>
<option value="CA">CA</option>
<option value="DE">DE</option>
<option value="FR">FR</option>
</select>';
  print '<input type="submit" value="search" name="search" />';
  print '</form>';
}

