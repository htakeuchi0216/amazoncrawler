# -*- coding: utf-8 -*-
# coding: utf-8
require 'pp'
require 'open-uri'
# Nokogiriライブラリの読み込み
require 'nokogiri'
require 'sqlite3'
#http://altarf.net/computer/ruby/1439
LOCKFILE = "/tmp/lock_file_name"
def file_check
  # ファイルチェック
  if File.exist?(LOCKFILE)
    # pidのチェック
      pid = 0
    File.open(LOCKFILE, "r"){|f|
      pid = f.read.chomp!.to_i
    }
    if exist_process(pid)
      p "other process is doing"
      exit
    else
      p "lock file is exists.but process is not doing.delete lockfile. "
      File.delete(LOCKFILE)
    end
   else
    # なければLOCKファイル作成
    File.open(LOCKFILE, "w"){|f|
      # LOCK_NBのフラグもつける。もしぶつかったとしてもすぐにやめさせる。
      locked = f.flock(File::LOCK_EX | File::LOCK_NB)
      if locked
        f.puts $$
      else
        p "ERROR!!!"
      end
    }
  end
end

def make_next_url(url,page=1)
  if url =~ /page=\d+/ then
    url = url.gsub(/page=\d+/,"page=#{page}")
    #print("page あります\n")
  else
    url += "&page=#{page}"
    #print("pageありません\n")
  end
  url
end

# プロセスの生き死に確認
def exist_process(pid)
  begin
    gid = Process.getpgid(pid)
    return true
  rescue => ex
    puts ex
    return false
  end
end

def get_base_data(url)
  body = ""
  p url
  open(
       url,
       "User-Agent" => "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0",
       #  "Referer" => "http://localhost/",
       #:proxy => 'http://127.0.0.1:8888'
       ) {|f|
    body =  f.read
  }
  per_page = 0
  max_count = 0
  doc = Nokogiri::HTML.parse( body, nil, "utf8")
  doc.css('#s-result-count').each do |node|
    if node.text =~ /1-([0-9]+) of ([0-9,]+) results/
    per_page = $1
      max_count = $2.delete(",")
    end
  end
  max_page = 0;
  doc.css('#pagn span').each do |node|
    if node.text =~ /^[0-9]+$/
      max_page = node.text
    end
  end

  return per_page,max_count,max_page
end

def get_product_by_scid(db,scid)
  cursor = db.execute("SELECT id FROM product WHERE search_condition_id = ? AND status =0 ",[scid] )
  array= []
  cursor.each do |tuple|
    array.push tuple[0]
  end
  array
end

def get_product_count_hash(db)
  cursor = db.execute("SELECT search_condition_id ,count(*) as cnt FROM product GROUP BY search_condition_id" )
  hash = {}
  cursor.each do |tuple|
    hash[ tuple[0] ] = tuple[1]
  end
  hash
end

def get_product_count(db,id)
  cursor = db.execute("SELECT count(*) as cnt FROM product WHERE search_condition_id = ? ", [id] )
  cursor.each do |tuple|
    return tuple[0]
  end
end

def get_list_complete(db,id,max_count=0)
  return 1
  # XXXXXXX
  p "11"
  gotten_cnt = get_product_count(db,id)
  p "22"
  return unless max_count

  if max_count.class.to_s != 'Fixnum'
    max_count = max_count.to_i
  end

  min = (max_count*0.9).to_i
  max = (max_count*1.1).to_i

  p "original:#{max_count}"
  p "min:#{min}"
  p "max:#{max}"
  p "gotten:#{gotten_cnt}"
  if min<= gotten_cnt && gotten_cnt <= max
    p "IT IS FINISH"
    return 1
  end

end

# 商品一覧の取得
def status1_action(db,id,url,done_page,max_page)

  p "STATUS1 action"
  #if max_page.nil?
  per_page,max_count,max_page = get_base_data(url)
  #end
  p "per_page : #{per_page} , max_count #{max_count}"

  p "==  done_page : #{done_page} ==========="
  p "==  max_page : #{max_page} ==========="

  p "==  done_page+1 : #{done_page+1} ==========="
  p "class:#{max_page.class.to_s}"

  cnt = 1;
  for num in (done_page+1)..(max_page.to_i) do
    p "num:#{num}"
    sleep(1)

    body = ""
    next_url = make_next_url(url,num)
    p "next: #{next_url}"
    open(
         next_url,
         "User-Agent" => "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0",
         ) {|f|
      body =  f.read
    }
    doc = Nokogiri::HTML.parse( body, nil, "utf8")

    result = []
    doc.css('a.s-access-detail-page').each do |node|
      puts "#{cnt}==#{node.text} #{node[:href]}"
      cnt = cnt + 1
      tmp = { "title"=> node.text, "href" => node[:href] }
      result.push tmp
      #hash = {"id" => tuple[0], "list_url" => tuple[1], "done_page" => tuple[2],"max_page"=>tuple[3],"status"=>tuple[4] }
    end

    if !result.empty?
      p "not empty"
      db.transaction
      begin
        if num == max_page
          db.execute("update search_condition SET done_page = ? ,status=3 WHERE id = ? ",[ num,id ])
        else
          db.execute("update search_condition SET done_page = ? WHERE id = ? ",[ num,id ])
        end
        #p "update crawle_version SET did_page = ? WHERE id = ? #{num},#{id}
        result.each{|a|
          p "INSERT : {a['title']}"
          db.execute("insert into product (search_condition_id,name,url,crawle_version) values(?,?,?,?) ",[ id , a["title"],a["href"],1 ])
        }

        p "DO COMMIT "
        db.commit    # 処理を確定する
      rescue
        p "ERRORXXX"
        db.rollback  # 例外が発生したら処理を取り消す
      end
    else
      p "can't get data"
      #MAXの前後10%以内の件数がすでに取得できていたら、完了とする
      if get_list_complete(db,id,max_count)
        p "change status: crawle list is finished"
        db.execute("update search_condition SET status=3 WHERE id = ? ",[ id ])
        db.commit    # 処理を確定する
      end
      exit
    end
  end

  if get_list_complete(db,id,max_count)
    p "change status: crawle list is finished"
    db.execute("update search_condition SET status=3 WHERE id = ? ",[ id ])
  end

  ## タイトルを表示
  #p doc.title

end

# 商品詳細の取得
def status3_action(db,id)
  p "===STATUS3 ACTION=== id :#{id}"
  ids = get_product_by_scid(db,id)
  ids.each{|pid|
    sleep(2)
    p "@@ pid: #{pid}"
    # phpのスクリプトを呼び出す
    ret = system("php /var/www/html/aznc/script/get_detail_from_api.php #{pid}")
    pp ret
  }
  p "get DETAIL COMPLETE XXX\n"
  db.transaction
  begin
    db.execute("update search_condition SET status=9 WHERE id = ? ",[ id ])
    db.commit    # 処理を確定する
  rescue
    p "ERROR22XXX"
    db.rollback  # 例外が発生したら処理を取り消す
  end
=begin
  detail_url= "http://www.amazon.com/Nike-Mens-RosheRun-Photo-511881-403/dp/B00DNNVJWE/ref=sr_1_2?s=apparel&ie=UTF8&qid=1430533156&sr=1-2&keywords=shoes"

  body = ""
  open(
       detail_url,
       "User-Agent" => "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:28.0) Gecko/20100101 Firefox/28.0",
       ) {|f|
    body =  f.read
  }
  #p body
  doc = Nokogiri::HTML.parse( body, nil, "utf8")
  p "[TITLE]:"+ doc.css('#productTitle').first.text
=end

end

###################################
###################################

file_check
sleep(5)
data = []
db = SQLite3::Database.new("/var/www/html/aznc/db/db2.sqlite3")

#count_hash = get_product_count_hash(db)
#p "=============="
#pp count_hash#XXX
#exit;

# db を使い MySQL を操作する
begin 
  cursor = db.execute("SELECT id , list_url,done_page,max_page, status
                      FROM  search_condition
                      WHERE status in( ? , ?, ? ) AND deleted = 0
                      ORDER BY id
                      LIMIT 1",
                      [1, 2, 3])
  cursor.each do |tuple|
    hash = {"id" => tuple[0], "list_url" => tuple[1], "done_page" => tuple[2],"max_page"=>tuple[3],"status"=>tuple[4] }
    data.push hash
  end
rescue SQLite3::SQLException
  print "execute()が失敗しました．\n"
end 

if data.empty?
   p "[ERROR]no data. exit."
  exit
end

pp data
status = data[0]["status"]
url = data[0]["list_url"]
id  = data[0]["id"]
done_page  = data[0]["done_page"]
max_page = data[0]["max_page"]

p " id : #{id} "
p "status ; #{status}"
#exit
case status
when 1
  # 商品一覧の取得
  status1_action(db,id,url,done_page,max_page)
  p "111"
when 2
  # 商品詳細の取得
  p "222"
when 3
  status3_action(db,id)
  p "333"
else
  p "UNKNOWN"
end
exit

# 終了時のLOCKFILE削除
File.delete(LOCKFILE)
