<?php
require_once 'goutte.phar';
require_once 'lib/Amazon.php';
use Goutte\Client;
error_reporting(-1);#XXX
# TODO:
# 画像リサイズXXX

# - 毎週のバッチの開始時に実行
# php index.php start_crawle
# - 商品データの取得
# php index.php crawle_detail

$db = get_db();
$client = new Client();

if($_SERVER["REQUEST_METHOD"] == "POST"){
  if( $_POST['mode'] == "update_status" ){
    $stmt = $db->prepare("UPDATE search_condition SET status=1 WHERE id = :id ");
    $stmt->bindValue(':id', $_POST['id'] , SQLITE3_INTEGER);
    if($result = $stmt->execute()){
      print "updated!!";
    }else{
      print "Update FAILED!";
    }
  }
  elseif( $_POST['mode'] == "update_count" ){
    print "ID-- : ".$_POST['id'];
    #exit();
    #print "UPDATE COOUNTE";exit();
    update_max_count_and_page($db,$client,$_POST['id']);
  }
  elseif( $_POST['mode'] == "delete" ){
    $stmt = $db->prepare("UPDATE search_condition SET deleted=1 WHERE id = :id ");
    $stmt->bindValue(':id', $_POST['id'] , SQLITE3_INTEGER);
    if($result = $stmt->execute()){
      print "deleted!!";
    }else{
      print "delete FAILED!";
    }
  }
  else{
    save_search_condition($db);
    $id = $db->lastInsertRowID();
    #print "LAST INSERTID: $id ";

    $client = new Client();
    update_max_count_and_page($db,$client,$id);
  }
}
print_form($db);

#main($argv);
function main($argv){
  $db = get_db();
    #print "db: ".  get_class($db)."---<Br/>";

  $client = new Client();

  $mode = $argv[1];
  if($mode == 'start_crawle'){
    # 一覧ページから、タイトル,URLを取得しDBに格納する
    # 
    get_product_list($client);
  }
  elseif($mode == 'crawle_list'){
    # select
    # crawle
    # INSERT INTO product , or update
    # update crawle_version
  }
  elseif($mode == 'crawle_detail'){
    # 商品詳細データの取得
    update_product_data($client);
  }
  else{
  }
}

function save_search_condition($db){
  try {
    $stmt = $db->prepare("INSERT INTO search_condition (list_url) VALUES (:list_url)");

    $list_url = $_POST['list_url'];
    if( !$list_url ){
      print "URLが不正です";
      return ;
    }
    if (!preg_match("|^http://www.amazon.com|", $list_url)) {
      print "URLが不正です";
      return ;
    }

    $stmt->bindValue(':list_url', $list_url , SQLITE3_TEXT);
    if($result = $stmt->execute()){
      #print "SAVED!!";
    }else{
      print "SAVE FAILED!";
      #print $db->lastErrorMsg();
    }
  } catch (Exception $e) {
    echo '捕捉した例外: ',  $e->getMessage(), "\n";
  }
}
function print_form($db){

  $count_hash = array();
  $stmt = $db->query("SELECT search_condition_id ,status,count(*) as cnt FROM product WHERE deleted= 0 GROUP BY search_condition_id,status");
  while ($row = $stmt->fetchArray()) {
    $count_hash[$row['search_condition_id']][$row['status']] = $row['cnt'];
  }

  print "<table border=1>";
  print "<tr><td>id</td><td>list_url</td><td>per_page</td>";
  print "<td>max count</td><td>max page</td><td>done page</td>
          <td>取得待ち件数</td>
          <td>取得済み</td>
          <td>エラー件数</td>
          <td>status</td><td></td></tr>";
  $stmt = $db->query('SELECT * FROM search_condition WHERE deleted=0');
  while ($row = $stmt->fetchArray()) {
    print "<tr>";
    print '<td><a href="./list.php?id=' .$row['id'] .'">'.$row['id'].'</a></td>';
    $url = $row['list_url'];
    print "<td><a href=\"$url\" target=\"_blank\">";
    if(strlen($url) > 60 ){
      print substr($url, 0, 60)."...";
    }
    else{
      print $row['list_url'];
    }
    print "</a></td>";
    print "<td>".$row['per_page']."</td>";
    print "<td>".$row['max_count']."</td>";
    print "<td>".$row['max_page']."</td>";
    print "<td>".$row['done_page']."</td>";

    foreach ( array(0,1,9) as $n ){
      if( $count_hash[$row['id']][$n] ){
        print "<td>".$count_hash[$row['id']][$n] ."</td>";
      }else{
        print "<td> 0 </td>";
      }
    }

    switch ($row['status']) {
    case 0:
      echo "<td>登録状態";
      print <<< EOM
        <form action="./" method="POST">
        <input type="submit" value="クローリング待ちにする" />
        <input type="hidden" name="mode" value="update_status"/>
        <input type="hidden" name="id" value="{$row['id']}"/>
        </form>
        </td>
EOM;
      break;
    case 1:
      echo "<td>クローリング待ち状態</td>";
      break;
    case 2:
      echo "<td>商品一覧取得中</td>";
      break;
    case 3:
      print '<td><a href="./list.php?id=' .$row['id'] .'">商品詳細取得中</a></td>';
      break;
    #case 8:
    #  echo "<td>削除済み</td>";
    #  break;
    case 9:
      echo "<td>クローリング完了";
      echo '<a href="./download.php?id=' .$row['id'] .'">Download</a>';
      echo "</td>";
      break;
    }
    print <<< EOM
      <td>
      <form action="./" method="POST">
      <input type="submit" value="削除する" />
      <input type="hidden" name="mode" value="delete"/>
      <input type="hidden" name="id" value="{$row['id']}"/>
      </form>
      </td>
EOM;
    print "</tr>";
  }
print "</table>";

echo  <<< EOM
<form action="./index.php" method="POST">
 <p>
  <label for="list_url">list_url</label>
  <input type="input" name="list_url" id="list_url">
 </p>

 <input type="submit" value="submit"/>
</form>
EOM;

}