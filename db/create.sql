-- # status
-- # 0:登録後状態 (この状態で一度最大件数などを取得しに行く)
-- # 1:クローリング待ち状態
-- # 2:商品一覧取得クローリング実行中
-- # 3:商品詳細取得クローリング実行中
-- # 9:クローリング完了
create table search_condition(
       id integer primary key autoincrement,
       keyword     text,
       browse_node text,
       per_page    integer not null default 0,
       max_count   integer not null default 0,
       done_page   integer not null default 0,
       max_page    integer not null default 0,
       list_url    text,
       status     integer not null default 0,
       deleted  integer not null default 0,
       created_at default CURRENT_TIMESTAMP,
       updated_at default CURRENT_TIMESTAMP
);

-- status
-- 0 取得前状態
-- 1 取得完了
-- 9 ERROR
create table product(
       id integer primary key autoincrement,
       parent_id integer default null,
       search_condition_id integer not null,
       product_code text,
       name text,
       url  text,
       brand text,
       price integer not null default 0,
       parent_asin text,
       stock integer not null default 0,
       color text,
       size text,
       category text,
       asin text,
       status integer not null default 0,
       crawle_version integer not null ,
       deleted  integer not null default 0,
       created_at default CURRENT_TIMESTAMP,
       updated_at default CURRENT_TIMESTAMP,
       FOREIGN KEY (search_condition_id) REFERENCES search_condition(id) ,
       UNIQUE(search_condition_id,product_code)
);

-- ・ブランド名
--・タイトル
--・価格
--・在庫数
--・カラー
--・サイズ
--・画像
--・カテゴリー（Nike ? Clothing, Shoes & Jewelry ? Men）
--・ＡＳＩＮコード


-- #ALTER TABLE product ADD COLUMN prodkey text;
--ALTER TABLE product ADD COLUMN  brand text;
--ALTER TABLE product ADD COLUMN  stock integer not null default 0;
--ALTER TABLE product ADD COLUMN  color text;
--ALTER TABLE product ADD COLUMN  size text;
--ALTER TABLE product ADD COLUMN  category text;
--ALTER TABLE product ADD COLUMN  asin text;
--ALTER TABLE product ADD COLUMN  status integer not null default 0;
--ALTER TABLE product ADD COLUMN  price integer not null default 0;
--ALTER TABLE product ADD COLUMN  parent_asin text;
--ALTER TABLE product ADD COLUMN  parent_id integer defualt null;

-- INSERT INTO  search_condition (keyword,browse_node) VALUES("test","11111");
-- INSERT INTO  crawle_version (id,query,max_page) VALUES(1,"limit500",557);
