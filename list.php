<?php
require_once 'lib/Amazon.php';

$id = $_GET["id"];
if (!$id || !is_numeric($id) ){
    print "parameter error";
    exit;
}

$db = get_db();
$cols = array("id","parent_id","name","brand","stock","color","price","size","category","asin","parent_asin","status");
$stmt = $db->query("SELECT * FROM product WHERE search_condition_id= $id ");
print "<table border=1>";
print "<tr>";
foreach ($cols as $col){
  print "<td>$col</td>";
}
print "</tr>";
while ($row = $stmt->fetchArray()) {
  print "<tr>";
  foreach ($cols as $col){
    print "<td>".$row[$col]."</td>";
  }
  print "</tr>";
}
print "</table>";