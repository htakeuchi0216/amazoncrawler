<?php

require_once 'lib/Amazon.php';
$db = get_db();

# 空のディレクトリの削除
function remove_empty_directory($id){
  $cmd = "find /tmp/download/{id}/ -type d -empty -delete";
  #print $cmd;
  exec("zip $cmd");
}

# ディレクトリのzip圧縮
function zip_directory($id){
  $dir = "/tmp/download/$id/";
  $zip_name = $id.".zip";
  #print "<BR/>";
  #print "zip /tmp/download/{$zip_name} -r {$dir} <BR/>";
  exec("zip /tmp/download/{$zip_name} -r {$dir}");
}

function output_csv($items,$id,$dir){
  #print "OUTPUTCSV2222222222222";

  $head = "id,title,brand,price,stock,color,size,category,asin,parent_asin,status \n";
  $csvpath = "{$dir}/{$id}.csv";
  $fp = fopen($csvpath, "w");
  fwrite($fp, $head);
  $str ="";
  foreach ($items as $line){
    foreach ($line as $l){
      $str .= '"'.$l.'"'.',';
    }
    $str .="\n";
  }
  fwrite($fp, $str);
  fclose($fp);
}

$id = $_GET['id'];

$stmt = $db->query("SELECT * FROM product WHERE search_condition_id= $id ORDER BY parent_asin,id");
$cols = array("id","name","brand","price","stock","color","size","category","asin","parent_asin","status");
$lines = array();
while ($row = $stmt->fetchArray()) {
  $line = array();
  foreach ($cols as $col){
    array_push ($line, $row[$col]);
  }
  array_push ($lines, $line);
}


$dir = "/tmp/download/$id/";
$zip = "/tmp/download/$id".".zip";

if( !file_exists( $zip ) ){
  if( file_exists( $dir ) ){
    # CSV作成
    output_csv(	$lines,$id,$dir);
    # 空ディレクトリ削除
    remove_empty_directory($id);
    # ディレクトリ圧縮
    zip_directory($id);
  }else{
    print "DIRE IS NOT OK";
    exit();
  }
}

//パス
$fname = $id.".zip";
$fpath = '/tmp/download/'.$fname;

if( file_exists( $fpath ) ){
  header('Content-Type: application/force-download');
  header('Content-Length: '.filesize($fpath));
  header('Content-disposition: attachment; filename="'.$fname.'"');
  readfile($fpath);
}else{
  print "$fname は存在しません";
}