<?php

function search($page=1,$browse_node=""){
  $result = array();

  $params = array();
  $params['Operation'] = 'ItemSearch'; // ← ItemSearch オペレーションの例
  if($browse_node){// branch_node
    $params['BrowseNode'] = $browse_node;
    #$params['SearchIndex'] = 'Books'; // カテゴリXXX
    $params['SearchIndex'] = get_search_id_by_node_id($browse_node);

  }else{
    $params['SearchIndex'] = 'All'; // カテゴリXXX
  }

  //* MerchantId
  // 出品者を表すID。デフォルトでは全ての出品者の商品が対象となるが、「Amazon」を指定することでAmazon出品の商品のみが扱われる。
  //*Condition
  // Newで新品のみ
  //* Availability
  // 在庫有無

  #$params['Keywords'] = $keyword; // ← 検索ワード
  $params['ItemPage'] = $page;
  $params['ResponseGroup']= "Images,ItemAttributes,OfferSummary";
  $xml = _call_api($params);
  $hash = json_decode(json_encode($xml), true);
  if(!isset($hash['Items']) ){
    return $result;
  }
  $data = $hash['Items'];
  foreach ( $data['Item'] as $item){
    $result[] = $item;
  }
  print "TotalResults: ". $data['TotalResults'];

  # すでに１ページ目は取得しているので、2ページ目から取得する
  #$params['ItemPage'] = $p;
  $xml = _call_api($params);
  $hash = json_decode(json_encode($xml), true);
  if(!isset($hash['Items']) ){
    return $result;
  }
  foreach ( $hash['Items']['Item'] as $item){
    $result[] = $item;
  }
#print "{$p} :: count: ". count($result) . "\n";

  return $result;
}
